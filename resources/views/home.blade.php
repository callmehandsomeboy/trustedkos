@extends('layout')
@section('head_content')
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>home</title>
</head>
@endsection

@section('body_content')
<header>
 
		<h2>Blog MalasNgoding</h2>
		<nav>
			<a href="/home">HOME</a>
			|
            <a href="/blog">BLOG</a>
		</nav>
	</header>
@endsection