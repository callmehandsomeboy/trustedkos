<!DOCTYPE html>
<html lang="en">
@yield('head_content')
<body>
@yield('body_content')
</body>
</html>