<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Material Design for Bootstrap</title>
  <!-- MDB icon -->
  <link rel="icon" href="/mdb/img/mdb-favicon.ico" type="image/x-icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="/mdb/css/bootstrap.min.css">
  <!-- Material Design Bootstrap -->
  <link rel="stylesheet" href="/mdb/css/mdb.min.css">
  <!-- Your custom styles (optional) -->
  <link rel="stylesheet" href="/mdb/css/style.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-sm-6">
            <div class="card mt-5 mx-auto" style="width:30rem;">
                <div class="card-body">
                    <form action="" method="POST">
                        <input type = "hidden" name = "_token" value = "{{csrf_token()}}">
                        <center><img src="/asset/images/logo1.png" style="width:250px;" alt=""></center>
                        <!-- Email -->
                        <input type="text" name="username" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="Username">
                        <!-- Password -->
                        <input type="password" name="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="Password">
                        <!-- Sign in button -->
                        <button class="btn btn-info btn-block my-4" type="submit">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
</div>
 

  <!-- jQuery -->
  <script type="text/javascript" src="/mdb/js/jquery.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="/mdb/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="/mdb/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="/mdb/js/mdb.min.js"></script>
  <!-- Your custom scripts (optional) -->
  <script type="text/javascript"></script>

</body>
</html>
