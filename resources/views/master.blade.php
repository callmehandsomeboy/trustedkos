<html>

<head>
	<title>
    @yield('title_atas')
    </title>
</head>
<body>
 
	<header>
 
		<h2>Blog MalasNgoding</h2>
		<nav>
			<a href="/blog/tentang">TENTANG</a>
			|
			<a href="/blog/kontak">KONTAK</a> |
            <a href="/blog">BLOG</a>
		</nav>
	</header>
	<hr/>
	<br/>
	<br/>
 
	<!-- bagian judul halaman blog -->
	<!-- <h3> @yield('judul_halaman') </h3> -->
 
 
	<!-- bagian konten blog -->
	<!-- @yield('konten') -->
 
 
	<br/>
	<br/>
	<hr/>
	<footer>
		<p>&copy; <a href="https://www.malasngoding.com">www.malasngoding.com</a>. 2018 - 2019</p>
	</footer>
 
</body>
</html>