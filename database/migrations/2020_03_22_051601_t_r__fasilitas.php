<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TRFasilitas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TR_Fasilitas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idDetailLokasi')->unsigned();
            $table->integer('idMtFasilitas')->unsigned();
            $table->dateTime('createdAt',0);
            $table->dateTime('updatedAt',0)->nullable();
            $table->dateTime('deletedAt',0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TR_Fasilitas');
    }
}
