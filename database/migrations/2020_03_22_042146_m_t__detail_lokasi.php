<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MTDetailLokasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MT_detailLokasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('locationId')->unsigned();
            $table->string('name',150)->nullable();
            $table->integer('gender')->nullable();
            $table->text('address')->nullable();
            $table->string('maps',225)->nullable();
            $table->dateTime('createdAt',0);
            $table->dateTime('updatedAt',0)->nullable();
            $table->dateTime('deletedAt',0)->nullable();
            $table->foreign('locationId')->references('id')->on('MT_Lokasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MT_detailLokasi');
    }
}
