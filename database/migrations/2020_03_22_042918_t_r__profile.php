<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TRProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TR_Profile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->unsigned();
            $table->string('name',200)->nullable();
            $table->text('address')->nullable();
            $table->string('gender',30)->nullable();
            $table->string('email',200)->unique()->nullable();
            $table->string('phone',15)->nullable();
            $table->string('photo',225)->nullable();
            $table->string('ig',200)->nullable();
            $table->string('twitter',200)->nullable();
            $table->string('fb',200)->nullable();
            $table->string('wa',200)->nullable();
            $table->dateTime('createdAt',0);
            $table->dateTime('updatedAt',0)->nullable();
            $table->dateTime('deletedAt',0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TR_Profile');
    }
}
