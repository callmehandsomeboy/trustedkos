<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MTUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MT_Users',function (Blueprint $table){
            $table->increments('id');
            $table->string('username',100)->nullable();
            $table->string('password',100)->nullable();
            $table->string('email',200)->unique()->nullable();
            $table->integer('role')->nullable();
            $table->integer('isActive')->nullable();
            $table->dateTime('createdAt');
            $table->dateTime('updatedAt')->nullable();
            $table->dateTime('deletedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MT_Users');
    }
}
