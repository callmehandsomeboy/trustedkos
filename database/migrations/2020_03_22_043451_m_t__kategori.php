<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MTKategori extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MT_Kategori', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',150)->nullabel();
            $table->string('image',150)->nullable();
            $table->dateTime('createdAt',0);
            $table->dateTime('updatedAt',0)->nullable();
            $table->dateTime('deletedAt',0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MT_Kategori');
    }
}
