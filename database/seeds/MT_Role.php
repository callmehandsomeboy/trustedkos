<?php

use Illuminate\Database\Seeder;

class MT_Role extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('MT_Role')->insert([
            [
                'id' => 1,
                'name' =>'Super Admin',
                'createdAt' => new DateTime()
            ],
            [
                'id' => 2,
                'name' => 'Content Kos',
                'createdAt' => new DateTime()
            ],
            [
                'id' => 3,
                'name' => 'Content Design',
                'createdAt' => new DateTime()
            ]
        ]);
    }
}
