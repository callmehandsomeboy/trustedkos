<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
// route back
Route::get('/2020/04/15/login','authLogin@index');

Route::get('/register','authLogin@register');
Route::post('/register/proses','authLogin@registerProses');
Route::post('/login/proses','authLogin@proses');
//route front
Route::get('/home','HomeController@home');
Route::get('/blog','HomeController@blog');
Route::get('/login','loginController@index');
Route::post('/login','loginController@postData')->middleware('Role');
Route::get('/show/{name}','loginController@getData');
Route::get('/hapus','loginController@deleteSessionData');

