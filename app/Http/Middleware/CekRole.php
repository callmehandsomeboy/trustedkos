<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request; // agar bisa handle http request dari form input
use Closure;

class CekRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->username == "adam") {
            $a = "ini halaman adam";
            return $next($request);
        } 
        abort(404);
    }
}
