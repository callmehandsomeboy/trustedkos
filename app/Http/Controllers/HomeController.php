<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        return view('layout.front.app');
    }
    public function blog(){
        return view('blog');
    }
}
